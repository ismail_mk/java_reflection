package com.greatlearning.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Scanner;

public class ReflectionApp {

	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Pleae enter the class name");

		String className = sc.next();
		System.out.println(className);
		
		int choice;

		System.out.println("Select the menu option");
		System.out.println("--------------------------------------------------------------");
		System.out.println(
				" 1. Methods \n 2. Class \n 3. Subclasses \n 4. Parent Classes \n 5. Constructors \n 6. Data members");
		System.out.println("--------------------------------------------------------------");

		choice = sc.nextInt();
		
		FileManager fileManager = new FileManager();
		ReflectionAPI javaClass;
		int opt;
		
		switch (choice) {
		case 1:
			javaClass = new ReflectionAPI(className);
            Method[] methods = javaClass.getAllMethods();
            opt = FileOperation();
            if(opt == 1){
				fileManager.storeToFile(methods);
            }else if(opt == 2){
            	fileManager.readFromStoredLocation();
            }
			break;

		case 2:
			javaClass = new ReflectionAPI(className);
			System.out.println(javaClass.getClass());
			break;

		case 3:
            javaClass = new ReflectionAPI(className);
            System.out.println(javaClass.getSubClasses());
			break;

		case 4:
            javaClass = new ReflectionAPI(className);
            Class clas = javaClass.getParentClass();
            opt = FileOperation();
            if(opt == 1){
				fileManager.storeToFile(clas);
            }else if(opt == 2){
            	fileManager.readFromStoredLocation();
            }			
            break;
		case 5:
            javaClass = new ReflectionAPI(className);
            Constructor<?>[] constructors = javaClass.getAllConstructorDetails();
            opt = FileOperation();
            if(opt == 1){
				fileManager.storeToFile(constructors);
            }else if(opt == 2){
            	fileManager.readFromStoredLocation();
            }
            break;
		case 6:
            javaClass = new ReflectionAPI(className);
            Field[] fields = javaClass.getAllDataMembers();
            opt = FileOperation();
            if(opt == 1){
				fileManager.storeToFile(fields);
            }else if(opt == 2){
            	fileManager.readFromStoredLocation();
            }
            break;

		}

	}
	
    private static int FileOperation() {
        System.out.println("Do you want to see any other further information - ");
        System.out.println("press Yes to recheck the menu and No if you want to continue");
        @SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
        String option = scanner.next();
        switch (option) {
            case "Yes":
                System.out.println("1. Store information into file");
                System.out.println("2. To see all previous file created");
                System.out.println("0. Exit without storing");
                int choice = scanner.nextInt();
                return choice;
            case "No":
            	System.out.println("Exiting");
                break;
            default:
                System.out.println("Invalid option, so exiting.");
                break;
        }
        return 0;
    }

}
